// Fill out your copyright notice in the Description page of Project Settings.


#include "SortingActor.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>  

DECLARE_CYCLE_STAT(TEXT("Sorting algorithm"), STAT_PerformSorting, STATGROUP_SORTING)
// Sets default values
ASortingActor::ASortingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomMesh"));

	// TArray<int> elements[1000];
	for (int i = 0; i < 1000; i++)
	{
		unsortedArray[i].Add(FMath::Rand() % 1000);
	}
	
	// unsortedArray.Append(elements, ARRAY_COUNT(elements));
}

// Called when the game starts or when spawned
void ASortingActor::BeginPlay()
{
	Super::BeginPlay();

	{
		SCOPE_CYCLE_COUNTER(STAT_PerformSorting);

		InsertionSort(unsortedArray);
	}

}

// Called every frame
void ASortingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASortingActor::ShowOutput()
{
	UE_LOG(LogTemp, Warning, TEXT("Showing output"));
}

void ASortingActor::InsertionSort(TArray<int> array)
{
	// {
		// SCOPE_CYCLE_COUNTER()
	// }
	int size = unsortedArray.Num();

	int key, j;
    for (int i = 1; i < size; i++)
    {
        key = array[i];
        j = i;

        while (j > 0 && array[j-1] >= key)
        {
            array[j] = array[j-1];
            j -= 1;
        }
        array[j] = key;
    }

	for (int i = 0; i < size; i++)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::FromInt(array[i]));
		PrintString(FString::FromInt(array[i]));
	}
	// UE_LOG(LogClass, Log, TEXT("Names: %s"), *array[size].ToString());
}

void ASortingActor::PrintString(FString text)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(text));
}
