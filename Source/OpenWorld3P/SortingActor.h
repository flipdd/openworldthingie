// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SortingActor.generated.h"

DECLARE_STATS_GROUP(TEXT("SortingProfiller"), STATGROUP_SORTING, STATCAT_Advanced);
UCLASS()
class OPENWORLD3P_API ASortingActor : public AActor
{
	GENERATED_BODY()
	// int32 elementscopy[10];
	// int32 elements2copy[20];
	// int32 elements3copy[5];

	// int32 elements2[20] = {7, 5, 9, 4, 8, 6, 0, 3, 2, 1, 3, 8, 1, 0, 2, 7, 4, 5, 6, 9};
	// int32 elements3[5] = {2, 1, 6, 4, 9};

public:	
	// Sets default values for this actor's properties
	ASortingActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UPROPERTY(EditAnywhere, Category = "Mesh")
	UStaticMeshComponent* meshComponent;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void ShowOutput();

	// UPROPERTY(BlueprintReadWrite)
	TArray<int> unsortedArray[1000];

	UFUNCTION(BlueprintCallable)
	void InsertionSort(TArray<int32> array);

	void PrintString(FString text);

};
