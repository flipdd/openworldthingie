// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameModeBase.h"
#include "MyActor.h"
#include "UObject/ConstructorHelpers.h"
#include "MyGameStateBase.h"

AMyGameModeBase::AMyGameModeBase()
{
	// set default pawn class to our Blueprinted character
	// static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/OpenWorld3P/Characters/ZombieDude/Enemy"));
	// if (PlayerPawnBPClass.Class != NULL)
	// {
	// 	DefaultPawnClass = PlayerPawnBPClass.Class;
	// }

	PointsToWin = 5;
}

void AMyGameModeBase::OnTargetHit()
{
	if (AMyGameStateBase* GS = Cast<AMyGameStateBase>(GameState))
	{
		GS->Points++;

		if (GS->Points >= PointsToWin)
		{
			// GEngine->AddOnScreenDebugMessage()
			UE_LOG(LogTemp, Warning, TEXT("You won the game!"));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("You scored a point. You now have %d points"), GS->Points);
		}
	}
}
