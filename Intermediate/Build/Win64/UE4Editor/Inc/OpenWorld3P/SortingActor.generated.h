// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENWORLD3P_SortingActor_generated_h
#error "SortingActor.generated.h already included, missing '#pragma once' in SortingActor.h"
#endif
#define OPENWORLD3P_SortingActor_generated_h

#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInsertionSort) \
	{ \
		P_GET_TARRAY(int32,Z_Param_array); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->InsertionSort(Z_Param_array); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execShowOutput) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ShowOutput(); \
		P_NATIVE_END; \
	}


#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInsertionSort) \
	{ \
		P_GET_TARRAY(int32,Z_Param_array); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->InsertionSort(Z_Param_array); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execShowOutput) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ShowOutput(); \
		P_NATIVE_END; \
	}


#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASortingActor(); \
	friend struct Z_Construct_UClass_ASortingActor_Statics; \
public: \
	DECLARE_CLASS(ASortingActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OpenWorld3P"), NO_API) \
	DECLARE_SERIALIZER(ASortingActor)


#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASortingActor(); \
	friend struct Z_Construct_UClass_ASortingActor_Statics; \
public: \
	DECLARE_CLASS(ASortingActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OpenWorld3P"), NO_API) \
	DECLARE_SERIALIZER(ASortingActor)


#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASortingActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASortingActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASortingActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASortingActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASortingActor(ASortingActor&&); \
	NO_API ASortingActor(const ASortingActor&); \
public:


#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASortingActor(ASortingActor&&); \
	NO_API ASortingActor(const ASortingActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASortingActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASortingActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASortingActor)


#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_PRIVATE_PROPERTY_OFFSET
#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_10_PROLOG
#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_PRIVATE_PROPERTY_OFFSET \
	OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_RPC_WRAPPERS \
	OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_INCLASS \
	OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_PRIVATE_PROPERTY_OFFSET \
	OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_INCLASS_NO_PURE_DECLS \
	OpenWorld3P_Source_OpenWorld3P_SortingActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENWORLD3P_API UClass* StaticClass<class ASortingActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID OpenWorld3P_Source_OpenWorld3P_SortingActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
