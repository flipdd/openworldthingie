// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenWorld3P/SortingActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSortingActor() {}
// Cross Module References
	OPENWORLD3P_API UClass* Z_Construct_UClass_ASortingActor_NoRegister();
	OPENWORLD3P_API UClass* Z_Construct_UClass_ASortingActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_OpenWorld3P();
	OPENWORLD3P_API UFunction* Z_Construct_UFunction_ASortingActor_InsertionSort();
	OPENWORLD3P_API UFunction* Z_Construct_UFunction_ASortingActor_ShowOutput();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void ASortingActor::StaticRegisterNativesASortingActor()
	{
		UClass* Class = ASortingActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "InsertionSort", &ASortingActor::execInsertionSort },
			{ "ShowOutput", &ASortingActor::execShowOutput },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ASortingActor_InsertionSort_Statics
	{
		struct SortingActor_eventInsertionSort_Parms
		{
			TArray<int32> array;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_array;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_array_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::NewProp_array = { "array", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SortingActor_eventInsertionSort_Parms, array), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::NewProp_array_Inner = { "array", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::NewProp_array,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::NewProp_array_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "SortingActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASortingActor, nullptr, "InsertionSort", sizeof(SortingActor_eventInsertionSort_Parms), Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASortingActor_InsertionSort()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASortingActor_InsertionSort_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ASortingActor_ShowOutput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASortingActor_ShowOutput_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "SortingActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASortingActor_ShowOutput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASortingActor, nullptr, "ShowOutput", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASortingActor_ShowOutput_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ASortingActor_ShowOutput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASortingActor_ShowOutput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASortingActor_ShowOutput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASortingActor_NoRegister()
	{
		return ASortingActor::StaticClass();
	}
	struct Z_Construct_UClass_ASortingActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_meshComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASortingActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenWorld3P,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ASortingActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ASortingActor_InsertionSort, "InsertionSort" }, // 1430506751
		{ &Z_Construct_UFunction_ASortingActor_ShowOutput, "ShowOutput" }, // 1955961562
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASortingActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SortingActor.h" },
		{ "ModuleRelativePath", "SortingActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASortingActor_Statics::NewProp_meshComponent_MetaData[] = {
		{ "Category", "Mesh" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "SortingActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASortingActor_Statics::NewProp_meshComponent = { "meshComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASortingActor, meshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASortingActor_Statics::NewProp_meshComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASortingActor_Statics::NewProp_meshComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASortingActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASortingActor_Statics::NewProp_meshComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASortingActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASortingActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASortingActor_Statics::ClassParams = {
		&ASortingActor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ASortingActor_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_ASortingActor_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ASortingActor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ASortingActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASortingActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASortingActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASortingActor, 1583025951);
	template<> OPENWORLD3P_API UClass* StaticClass<ASortingActor>()
	{
		return ASortingActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASortingActor(Z_Construct_UClass_ASortingActor, &ASortingActor::StaticClass, TEXT("/Script/OpenWorld3P"), TEXT("ASortingActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASortingActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
